# Basics

### What is the CLI (Command Line Interface)

A Command Line Interface or CLI for short is a user interface that allows you to interact with computer programs that do not have a traditional Graphical User Interface or GUI.  This means that they are text only and all input to the program must be typed and entered.  To interact with Unicornis you must use the CLI there is no way around it.  Here are some links to get you started if you are unfamiliar with CLI.

* [Windows](https://www.cs.princeton.edu/courses/archive/spr05/cos126/cmd-prompt.html)

* [Linux (Ubuntu)](https://tutorials.ubuntu.com/tutorial/command-line-for-beginners#0)

---

### Getting Started

Now that we are familiar with the CLI we can get started!

Unicornis is a Smart Chain utilizing Komodo and their Antara Framework.  To use UNI our first step is downloading and starting the Komodo Software.

### Downloading

Because Unicornis is using some of Komodo's most recent Antara features we must use the development branch of the software.  You can [download](https://gitlab.com/unicornis-project/unicornis/-/releases) pre-built binaries or [build](../Build) them yourself.

### Sync the Komodo Blockchain

Once you have downloaded or built the software its time to sync the Komodo blockchain.  You have 2 options, first syncing from block zero.  This is considered the most secure because you are downloading the chain directly from network peers.  A drawback from this method is it will take some time, possibly more than 24 hours.  

The second option is to download the Blockchain *Bootstrap* hosted by [dexstats.io](https://eu.bootstrap.dexstats.info/KMD-bootstrap.tar.gz).  This is considered less secure because you a trusting a 3rd party and not the chain to maintain the integrity of the bootstrap file but it will have you up and running in 30 minutes compared to 24+ hours.

#### Preparation of base data directory

Before starting the software we must create a file called `komodo.conf`.  This file should go in default data folder for komodo.  For windows this is located at `%AppData%/komodo`. You can get to the AppData folder by using the ` ⊞ ` Windows key and entering `%AppData%` and pressing enter. If using Linux it will be located in your home folder under `.komodo` (Note the `.`).  

In both cases if the Komodo folder is not present please go ahead and create it.  

!!! note
    If you wish to use a custom data directory you can by adding the startup flag `-datadir=path/to/folder`

Once you have located/created the base data folder create a file called `komodo.conf`.  Nothing needs to go in the file it just needs to be there when the daemon starts.  If using windows make sure to save the file with the `All Files` type.

#### Fetch Zcash parameters

Once you are finished preparing the base data directory you must fecth the Zcash parameters that give Smart Chains the ability to send private transactions.  To do this use the provided scripts for [Windows](https://gitlab.com/unicornis-project/unicornis/raw/master/zcutil/fetch-params.bat) or [Linux](https://gitlab.com/unicornis-project/unicornis/raw/master/zcutil/fetch-params.sh)

#### Option One (Sync from zero)

Ok now that we have prepared our base data directory and fetched the Zcash parameters we can attempt to sync from zero. All we need to do is run `komodod` for this. 

!!! note
    If you wish to see more data such as the blocks syncing run komodod with the startup flag `-printtoconsole` 

As mentioned this **WILL** take some time so go do something else and come and check back periodically.

If you didn't use the startup flag `-printtoconsole` you can check the status of the blockchain by using `komodo-cli getinfo`

!!! example
    **`komodo-cli getinfo`**

    ```
      "version": 2001526,
      "protocolversion": 170007,
      "KMDversion": "0.4.0a",
      "synced": false,
      "notarized": 0,
      "prevMoMheight": 0,
      "notarizedhash": "0000000000000000000000000000000000000000000000000000000000000000",
      "notarizedtxid": "0000000000000000000000000000000000000000000000000000000000000000",
      "notarizedtxid_height": "mempool",
      "notarized_confirms": 0,
      "walletversion": 60000,
      "interest": 0.00000000,
      "balance": 0.00000000,
      "blocks": 3730,
      "longestchain": 0,
      "tiptime": 1474716227,
      "difficulty": 1,
      "keypoololdest": 1570154290,
      "keypoolsize": 101,
      "paytxfee": 0.00000000,
      "sapling": -1,
      "timeoffset": 0,
      "connections": 3,
      "proxy": "",
      "testnet": false,
      "relayfee": 0.00000100,
      "errors": "",
      "name": "KMD",
      "p2pport": 7770,
      "rpcport": 7771
    ```

Once it shows `"synced": true` you are ready to move to the [next step](#)

#### Option two (using a bootstrap)

If you don't have the time to wait for the blockchain to sync from zero you can use a [bootstrap](https://eu.bootstrap.dexstats.info/KMD-bootstrap.tar.gz) to dramatically speed up the process.  Once Downloaded you must extract the contents of the file to the komodo base folder.  As mentioned earlier this is located at `%AppData%\komodo` for Windows users and `~/.komodo` for linux users. If you are a windows user we suggest [7zip](https://www.7-zip.org/) as a tool to extract the bootstrap files to the komodo base directory.  Linux users should use `tar xvzf`.

Once the contents of the download have been extracted you can start komodod.  Again all you need is `komodod`.  Use `komodo-cli getinfo` to check on its status.  The process of syncing should be fairly quick.

---

### Connecting to the Unicornis Network

Now its time to connect to the Unicornis Network!

The command to do so is quite a mouthful so feel free to copy and paste or use the provided startup scripts for [Windows](#) or [Linux](#).

!!! warning
    **You Must enter all these parameters properly or you will not be able to connect to the Unicornis Network!**

```
  komodod -ac_name=UNICORNIS -ac_supply=315000 -ac_adaptivepow=1 -ac_blocktime=60 -ac_cbmaturity=1 -ac_cc=2019 -ac_ccactivate=1000 -ac_eras=5 -ac_reward=115000000,8800000,4200000,1800000,1250000 -ac_end=525600,1051200,1576800,1680481,19867680 -ac_staked=50 -addnode=34.223.52.215 -addnode=34.223.107.167 -addnode=34.222.194.199
```

To check on the status of the Unicornis Blockchain syncing use `komodo-cli -ac_name=UNICORNIS getinfo`
