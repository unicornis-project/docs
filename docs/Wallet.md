# Using the Komodo/Unicornis CLI Wallet

For the time being please refer to the [Komodo Antara Smart Chain API](https://developers.komodoplatform.com/basic-docs/smart-chains/smart-chain-api/wallet.html#addmultisigaddress) Documentation for how to interact with the wallet.