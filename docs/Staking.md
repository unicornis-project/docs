# Staking

Unicornis Blocks are 50% mined by Proof of Work (PoW) and 50% by Proof of Stake (PoS).  Komodo Smart Chains use a POS64 implementation and if you would liek to learn more you can visit [here](https://docs.komodoplatform.com/komodo/assetchain-params.html#ac-staked)

The documentation below explains how to use a tool called 'pos64splitter` to properly stake a Unicornis Node.

# pos64splitter

First things first, lets install the dependencies. 

## Dependencies
```shell
sudo apt-get install python3-dev
sudo apt-get install python3 libgnutls28-dev libssl-dev
sudo apt-get install python3-pip
pip3 install setuptools
pip3 install wheel
pip3 install base58 slick-bitcoinrpc
```

[komodod](https://gitlab.com/unicornis-project/unicornis) installed with your assetchain running.

## How to Use

`git clone https://gitlab.com/unicornis-project/pos64staker`

`cd pos64staker`

`./genaddresses.py`
```shell
Please specify chain:UNICORNIS
```

This will create a `list.json` file in the current directory. **THIS FILE CONTAINS PRIVATE KEYS. KEEP IT SAFE.**
Copy this file to the directory `komodod` is located. 

`cp list.json ~/komodo/src/list.json`

`./sendmany64.py`
```shell
Please specify chain:UNICORNIS
Balance: 1000000.77
Please specify the size of UTXOs:10
Please specify the amount of UTXOs to send to each segid:10
```
Please take note of what this is actually asking for. The above example will send 6400 coins total. It will send 100 coins in 10 UTXOs to each of the 64 segids. Will throw error if your entered amounts are more than your balance. Will tell you how much available you have for each segid.

You now need to start the daemon with -blocknotify and -pubkey set.

Fetch a pubkey from your `list.json` and place it in your start command. 

!!! example

   ```
     ./komodod -ac_name=UNICORNIS -ac_supply=315000 -ac_adaptivepow=1 -ac_blocktime=60 -ac_cbmaturity=1 -ac_cc=2019 -ac_ccactivate=1000 -ac_eras=5 -ac_reward=115000000,8800000,4200000,1800000,1250000 -ac_end=525600,1051200,1576800,1680481,19867680 -ac_staked=50 -addnode=34.223.52.215 -addnode=34.223.107.167 -addnode=34.222.194.199 -pubkey=0367e6b61a60f9fe6748c27f40d0afe1681ec2cc125be51d47dad35955fab3ba3b '-blocknotify=/home/<USER>/pos64staker/staker.py %s UNICORNIS'
   ```

**NOTE the single quotes!**

After the daemon has started and is synced simply do `komodo-cli -ac_name=UNICORNIS setgenerate true 0` to begin staking. 


### How the staker.py works

on block arrival:

getinfo for -pubkey 

setpubkey for R address 

check coinbase -> R address 

if yes check segid of block.

if -1 send PoW mined coinbase to :

        listunspent call ... 

        sort by amount -> smallest at top and then by confirms -> lowest to top. (we want large and old utxos to maximise staking rewards.)

        select the top txid/vout

        add this txid to txid_list

        get last segid stakes 1440 blocks (last24H)

        select all segids under average stakes per segid in 24H

        randomly choose one to get segid we will send to.        

if segid >= 0 :

    fetch last transaction in block

    check if this tx belongs to the node

    if yes, use alrights code to combine this coinbase utxo with the utxo that staked it.
    
    
### Withdraw 

Withdraw script is for withdrawing funds from a staking node, without messing up utxo distribution. Works like this:

    Asks for percentage you want locked (kept). 
    
    It then counts how many utxo per segid. 
    
    Locks the largest and oldest utxos in each segid up to the % you asked.
    
    Gives balance of utxos remaning that are not locked.  These should be the smallest and newest utxo's in each segid. The least likely to stake.
    
    Then lets you send some coins to an address. 
    
    Unlocks utxos again.


### Check if you are staking properly

To check you are staking properly on all 64 segid's you can use the command `komodo-cli -ac_name=UNICORNIS getbalance64`. The output should look like this:

!!! example
    **Note Some of this info is truncated**

    ```
      "mature": 2552.75666379,
      "immature": 85.65045000000001,
      "staking": [
      86895605815,
      2575000000,
      3690000000,
      4460005000,
      1345000000,
      4575005000,
      3230000000,
      2460000000,
      2460005000,
      2115000000,
      3460000000,
      345000000,
      2230000000,
      3345000000,
      3115000000,
      3230000000,
      2575000000,
      2230000000,
      1920018555,
      115000000,
      5345000000,
      2345005000,
      2345000000,
      2805000000,
      1460000000,
      115000000,
      3690000000,
      1805000000,
      3230000000,
      3460000000,
      3460000000,
      2345000000,
      2460000000,
      3230000000,
      2230000000,
      2230000000,
      3230000000,
      2000000000,
      4345000000,
      230000000,
      5230000000,
      4575000000,
      1460000000,
      3575012009,
      460000000,
      2460000000,
      3115000000,
      2575000000,
      1345000000,
      4230000000,
      2345000000,
      4920000000,
      4345000000,
      1345005000,
      2690005000,
      4575000000,
      115000000,
      2230000000,
      3345000000,
      4345000000,
      2805000000,
      1115000000,
      1000000000,
      2345000000
      ],
     "notstaking": [
      2990045000,
      0,
      0,
      115000000,
 
      ...

      0,
      115000000
      ]
    ```
